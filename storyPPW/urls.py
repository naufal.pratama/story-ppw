"""storyPPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from django.contrib import admin
from profilepage.views import profile
from profilepage.views import gallery
from profilepage.views import register
from profilepage.views import schedule
from profilepage.views import add_schedule
from profilepage.views import drop_schedule

urlpatterns = [
	path('admin/', admin.site.urls),
	path('profile/', profile, name='profile'),
	path('gallery/', gallery, name='gallery'),
	path('register/', register, name='register'),
    path('schedule/', schedule, name='schedule'),
    path('add_schedule/', add_schedule, name='add_schedule'),
	path('drop_schedule/', drop_schedule, name='drop_schedule'),
]
