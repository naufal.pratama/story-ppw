from django.conf.urls import url
from .views import profile
from .views import gallery
from .views import register
from .views import schedule
from .views import add_schedule
from .views import drop_schedule

urlpatterns = [
	url(r'^$', profile, name='profile'),
	url(r'^$', gallery, name='gallery'),
	url(r'^$', register, name='register'),
	url(r'^$', schedule, name='schedule'),
	url(r'^$', add_schedule, name='add_schedule'),
	url(r'^$', drop_schedule, name='drop_schedule'),
]
