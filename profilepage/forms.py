from django import forms
from .models import EventEntry

class EventForm(forms.Form):
    activity = forms.CharField(label='Activity', required=True, max_length=100)
    date = forms.DateField(label='Date', required=True, widget=forms.DateInput(attrs={'type' : 'date'}))
    time = forms.TimeField(label='Time', required=True, widget=forms.TimeInput(attrs={'type' : 'time'}))
    place = forms.CharField(label='Place', required=True, max_length=100)
    category = forms.CharField(label='Category', required=True, max_length=100)