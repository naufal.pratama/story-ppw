from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import EventForm
from .models import EventEntry

def profile(request):
	response = {}
	return render(request,'profile.html',response)
	
def gallery(request):
	response = {}
	return render(request, 'gallery.html', response)
	
def register(request):
	response = {}
	return render(request,'register.html',response)

def schedule(request):
	full_sched = EventEntry.objects.all()
	response = {
		"schedules" : full_sched
	}
	return render(request,'schedule.html',response)

def drop_schedule(request):
	EventEntry.objects.all().delete()
	return HttpResponseRedirect("/schedule")
	
def add_schedule(request):
	form = EventForm(request.POST or None)
	response = {}
	if (request.method == "POST"):
		if (form.is_valid()):
			activity = request.POST.get('activity')
			date = request.POST.get('date')
			time = request.POST.get('time')
			place = request.POST.get('place')
			category = request.POST.get('category')
			eventEntry = EventEntry(activity=activity, date=date, time=time, place=place, category=category)
			eventEntry.save()
			return HttpResponseRedirect("/schedule")
		else:
			return render(request, "add_schedule.html", response)
	response['form'] = form
	return render(request,'add_schedule.html',response)