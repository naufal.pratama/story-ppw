from django.contrib import admin
from .models import EventEntry

# Register your models here.
admin.site.register(EventEntry)