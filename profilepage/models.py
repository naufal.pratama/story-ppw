from django.db import models

# Create your models here.
class EventEntry(models.Model):
    activity = models.CharField(max_length = 100)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length = 100)
    category = models.CharField(max_length = 100)

#hari, tanggal, jam, nama kegiatan, tempat, dan kategori